import React, { Component } from "react";
import { Modal, Button } from "antd";

export default class ModalConfirm extends Component {
  render() {
    return (
      <Modal
        title="Gio hang"
        centered
        width={1000}
        visible={this.props.isOpenModal}
        onOk={this.props.handleOpenModal}
        onCancel={this.props.handleCloseModal}
      >
        <table class="table">
          <thead></thead>
          <tbody>
            <tr>
              <td>Mã sản phẩm</td>
              <td>Hình ảnh</td>
              <td>Tên sản phẩm</td>
              <td>Số lượng</td>
              <td>Đơn giá</td>
              <td>Thành tiền</td>
              <td></td>
            </tr>

            {this.props.cart.map((item) => {
              return (
                <tr>
                  <td>{item.maSP}</td>
                  <td>
                    <img style={{ width: "50px" }} src={item.hinhAnh} alt="" />
                  </td>
                  <td>{item.tenSP}</td>
                  <td>
                    <button
                      onClick={() => {
                        this.props.handleTangGiamSoLuong(item, 1);
                      }}
                      style={{ width: "20px", padding: "0" }}
                      className="btn btn-primary container"
                    >
                      +
                    </button>
                    <span className="px-1">{item.soluong}</span>
                    <button
                      onClick={() => {
                        this.props.handleTangGiamSoLuong(item, -1);
                      }}
                      style={{ width: "20px", padding: "0" }}
                      className="btn btn-primary"
                    >
                      -
                    </button>
                  </td>
                  <td>{item.giaBan}</td>
                  <td>{item.soluong * item.giaBan}</td>
                  <td>
                    <button
                      onClick={() => {
                        this.props.handleXoaSp(item);
                      }}
                      className="btn btn-danger"
                    >
                      Xoá
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </Modal>
    );
  }
}
