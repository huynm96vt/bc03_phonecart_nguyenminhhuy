import React, { Component } from "react";
import ModalConfirm from "./ModalConfirm";
import ProductItem from "./ProductItem";

export default class ProductList extends Component {
  render() {
    return (
      <div className="container">
        <ModalConfirm
          isOpenModal={this.props.isOpenModal}
          handleOpenModal={this.props.handleOpenModal}
          handleCloseModal={this.props.handleCloseModal}
          cart={this.props.cart}
          handleTangGiamSoLuong={this.props.handleTangGiamSoLuong}
          handleXoaSp={this.props.handleXoaSp}
        />
        <div className="row">
          {this.props.productList.map((item, index) => {
            return (
              <ProductItem
                handleXemChiTiet={this.props.handleXemChiTiet}
                key={index}
                data={item}
                handleOpenModal={this.props.handleOpenModal}
                handleCloseModal={this.props.handleCloseModal}
                handleAddToCart={this.props.handleAddToCart}
                setIsShowDetail={this.props.setIsShowDetail}
              />
            );
          })}
        </div>
      </div>
    );
  }
}
