import React, { Component } from "react";
import { mangSanPham } from "./dataPhone";
import ProductList from "./ProductList";

export default class PhoneCart extends Component {
  state = {
    productList: mangSanPham,
    productDetail: {},
    isOpenModal: false,
    cart: [],

    isShowDetail: false,
    showDetail: () => {
      if (this.state.isShowDetail == true) {
      }
    },
  };

  handleXemChiTiet = (data) => {
    let cloneproductDetail = { ...data };
    this.setState({ productDetail: cloneproductDetail });
  };

  handleOpenModal = () => {
    this.setState({ isOpenModal: true });
  };

  handleCloseModal = () => {
    this.setState({ isOpenModal: false });
  };

  handleAddToCart = (sanpham) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => {
      return item.maSP == sanpham.maSP;
    });
    if (index == -1) {
      let newSP = { ...sanpham, soluong: 1 };
      cloneCart.push(newSP);
    } else {
      cloneCart[index].soluong++;
    }
    this.setState({ cart: cloneCart });
  };

  handleTangGiamSoLuong = (sanpham, giatri) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => {
      return item.maSP == sanpham.maSP;
    });
    cloneCart[index].soluong += giatri;
    cloneCart[index].soluong == 0 && cloneCart.splice(index, 1);
    this.setState({ cart: cloneCart });
  };

  handleXoaSp = (sanpham) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => {
      return item.maSP == sanpham.maSP;
    });
    cloneCart.splice(index, 1);
    this.setState({ cart: cloneCart });
  };

  setIsShowDetail = () => {
    this.setState({ isShowDetail: true });
  };

  handleShowDetail = () => {
    if (this.state.isShowDetail == true) {
      return (
        <table class="table">
          <thead>
            <tr>
              <td>
                <h3>Thông số kỹ thuật</h3>
              </td>
              <td></td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Màn hình</td>
              <td>{this.state.productDetail.manHinh}</td>
            </tr>
            <tr>
              <td>Hệ điều hành</td>
              <td>{this.state.productDetail.heDieuHanh}</td>
            </tr>
            <tr>
              <td>Camera trước</td>
              <td>{this.state.productDetail.cameraTruoc}</td>
            </tr>
            <tr>
              <td>Camera sau</td>
              <td>{this.state.productDetail.cameraSau}</td>
            </tr>
            <tr>
              <td>Ram</td>
              <td>{this.state.productDetail.ram}</td>
            </tr>
            <tr>
              <td>Rom</td>
              <td>{this.state.productDetail.rom}</td>
            </tr>
            <tr>
              <td>Giá</td>
              <td>{this.state.productDetail.giaBan}</td>
            </tr>
          </tbody>
        </table>
      );
    }
  };

  render() {
    return (
      <div>
        <div></div>
        <ProductList
          handleXemChiTiet={this.handleXemChiTiet}
          productList={this.state.productList}
          isOpenModal={this.state.isOpenModal}
          handleOpenModal={this.handleOpenModal}
          handleCloseModal={this.handleCloseModal}
          handleAddToCart={this.handleAddToCart}
          cart={this.state.cart}
          handleTangGiamSoLuong={this.handleTangGiamSoLuong}
          handleXoaSp={this.handleXoaSp}
          setIsShowDetail={this.setIsShowDetail}
        />
        <div className="row py-5">
          <div className="col-4 text-center">
            <h2>{this.state.productDetail.tenSP}</h2>
            <img
              src={this.state.productDetail.hinhAnh}
              alt=""
              style={{ width: "100%" }}
            />
          </div>
          <div className="col-8">{this.handleShowDetail()}</div>
        </div>
      </div>
    );
  }
}
