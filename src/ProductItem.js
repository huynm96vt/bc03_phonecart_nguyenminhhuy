import React, { Component } from "react";

export default class ProductItem extends Component {
  render() {
    return (
      <div className="card col-4">
        <img src={this.props.data.hinhAnh} className="card-img-top" alt="..." />
        <div className="card-body">
          <h5 className="card-title">{this.props.data.tenSP}</h5>
          <button
            className="btn btn-success mx-3"
            onClick={() => {
              this.props.setIsShowDetail();
              this.props.handleXemChiTiet(this.props.data);
            }}
          >
            Xem chi tiết
          </button>
          <button
            className="btn btn-danger"
            onClick={() => {
              this.props.handleAddToCart(this.props.data);
              this.props.handleOpenModal();
            }}
          >
            Thêm giỏ hàng
          </button>
        </div>
      </div>
    );
  }
}
